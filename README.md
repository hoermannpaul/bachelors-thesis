# Bachelors thesis automatic picture classification and analysis with AI tools
## Folder Structure
* doc: latex source
* src: Pimcore command
* scripts: little helper scripts
* python: training logic
## How to use
1. install dependencies:
	* python3.6
	* pip packages: tensorflow, tensorflow-hub
2. setup pimcore command on target system
3. use provided scripts/setup.sh script to create folder structure, or create your own and set the paths in the command
4. to see usage run console pimcore:tensorflow -h
### Sample calls
`console pimcore:tensorflow listModels`

`console pimcore:tensorflow predict -N test1 -m 0 -i 347 -i 229 -i 231`

`console pimcore:tensorflow train -N test1 -m 0 -t parentTag`
