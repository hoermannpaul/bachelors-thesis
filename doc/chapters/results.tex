\chapter{Results}\label{cha:tests}
In the following, all measurements taken for this thesis are presented. For the original and Flickr data
all categories were used, for the Imagenet data only 6 of the 7 available ones are tested, because the corresponding
synset could not be found in a reasonable amount of time. This of course means that the performance
can't be compared directly, but can be taken as guidance. To evaluate performance on the data really used, meaning
performance on the original data, the training script is manipulated in such a way that
the training and test-data belong to the corresponding training data, but the validation data is the original data
every time. This means that when testing the Inception V3 network with Flickr or Imagenet data, the validation performance
is measured only with the original data. In the figures below $\varnothing$ 
denotes mean average and $\sigma$ denotes standard deviation. The number of training
steps varies because of the different amount of images. For tests with the original
test-data, 1.000 training steps were executed, for tests with Imagenet data and 
Flickr data, 10.000 steps were executed.  

\section{Test Environment}
All tests are executed on a PC with technical specifications shown in figure~\ref{tab:specs}.
The installed
operating system is Arch Linux\footnote{\url{https://www.archlinux.org/}}
in the 64 bit variant. The GPU driver
used is the proprietary one provided by
nvidia\footnote{GPU manufacturer, \url{https://www.nvidia.com/}} in version 418.43\-7.

\begin{table}[H]
	\centering
	\begin{tabularx}{.45\textwidth}{l X l}
		\toprule
		Part & Model \\
		\midrule
		CPU  & Ryzen2 5 \\
		RAM  & Corsair 32 GB 3000 MHz \\
		GPU  & Nvidia RTX 2080 8 GB \\
		SSD  & SanDisk Ultra 3 240 GB \\
		\bottomrule
	\end{tabularx}
	\caption[Technical specifications of the test-machine.]{Technical
		specifications of the test-machine.}\label{tab:specs}
\end{table}

\section{CPU vs GPU}
In figure~\ref{tab:trainperf} the results of the time measurements of training on CPU and
GPU can be seen, in figure~\ref{fig:cpugpu} a graphic visualization of the 
results can be seen.  

\begin{table}[H]
	\centering
	\begin{tabularx}{.68\textwidth}{r c c c c }
		\toprule
		Network      & $\varnothing$ GPU [s] & $\sigma$ [s] & $\varnothing$ CPU [s] & $\sigma$ [s] \\
		\midrule
		PNASNet      &    34.7  &    0.577  &  33.3  & 1.155     \\
		Inception V3 &    31.3  &    0.577  &  31.7  & 0.577     \\
		MobileNet V2 &    32.3  &    1.528  &  32.0  & 1.000     \\
		\bottomrule
	\end{tabularx}
	\caption[Time measurements CPU vs GPU.]{Time measurements of training on GPU vs CPU.}\label{tab:trainperf}
\end{table}

\begin{figure}[H]
	\centering
	{\noindent\includegraphics[width=0.75\textwidth]{../images/cpuvsgpu.png}}
	\caption[Visualization of CPU and GPU time measurements.]
	{Visualization of CPU and GPU time measurements.}\label{fig:cpugpu}
\end{figure}

\section{Inception v3}

\tocless\subsection{Original Data}
For the results in figure~\ref{tab:i_o}, 214 images are used. Of those 23 belong to
category foyer, 21 to hall, 50 to corridor, 25 to meetingroom, 23 to operating theatre, 41 to 
outside and 31 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$ &	$\sigma$ \\
		\midrule
		training accuracy         & 1.000 & 1.000 & 1.000 & 1.000     & 0         \\
		validation accuracy       & 0.760 & 0.760 & 0.630 & 0.717     & 0.075     \\
		\midrule
		cross entropy, training   & 0.009 & 0.009 & 0.010 & 0.009     & 0.001     \\
		cross entropy, validation & 0.605 & 0.790 & 1.048 & 0.814     & 0.223     \\
		\midrule
		time needed [s]               & 31.0   & 31.0   & 32.0   & 31.3   & 0.577     \\
		\bottomrule
	\end{tabularx}
	\caption[Measurements of inception v3 with original test-data.]{Measurements of three training runs of Inception v3
		with the original test-data, taken from log output with tensorboard.}\label{tab:i_o}
\end{table}

\tocless\subsection{Imagenet Data}
For the results in figure~\ref{tab:i_i}, 5.187 images are used.
Of those 648 belong to
category foyer, 721 to hall, 1.029 to meetingroom, 867 to operating theatre, 1.236 to 
outside and 686 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.830	&	0.770	&	0.850	&	0.817	&	0.042	\\
		validation accuracy	&	0.670	&	0.730	&	0.720	&	0.707	&	0.032	\\
		\midrule											
		cross entropy, training	&	0.532	&	0.471	&	0.383	&	0.462	&	0.075	\\
		cross entropy, validation	&	0.924	&	0.673	&	0.792	&	0.796	&	0.126	\\
		\midrule											
		time needed [s]	&	321.0	&	325.0	&	316.0	&	320.667	&	4.509	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of inception v3 with Imagenet test-data.]{Measurements of three training runs of Inception v3
		with the Imagenet test-data, taken from log output with tensorboard.}\label{tab:i_i}
\end{table}

\tocless\subsection{Flickr Data}
For the results in figure~\ref{tab:i_f}, 7.864 images are used.
Of those 1282 belong to corridor, 1112 to
category foyer, 1104 to hall,  1052 to meetingroom, 1136 to operating theatre, 1120 to 
outside and 1058 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.930	&	0.940	&	0.880	&	0.917	&	0.032	\\
		validation accuracy	&	0.640	&	0.570	&	0.550	&	0.587	&	0.047	\\
		\midrule											
		cross entropy, training	&	0.321	&	0.309	&	0.335	&	0.322	&	0.013	\\
		cross entropy, validation	&	1.419	&	1.632	&	1.419	&	1.490	&	0.123	\\
		\midrule											
		time needed [s]	&	322.0	&	324.0	&	320.0	&	322.0	&	2.000	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of inception v3 with Flickr test-data.]{Measurements of three training runs of Inception v3
		with the Flickr test-data, taken from log output with tensorboard.}\label{tab:i_f}
\end{table}

\section{MobileNet V2}
\tocless\subsection{Original Data}
For the results in figure~\ref{tab:m_o}, 214 images are used. Of those 23 belong to
category foyer, 21 to hall, 50 to corridor, 25 to meetingroom, 23 to operating theatre, 41 to 
outside and 31 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c c}
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$ &	$\sigma$ \\
		\midrule											
		training accuracy	&	1.000	&	1.000	&	1.000	&	1.000	&	0	\\
		validation accuracy	&	0.710	&	0.720	&	0.670	&	0.700	&	0.026	\\
		\midrule
		cross entropy, training	&	0.005	&	0.006	&	0.005	&	0.005	&	0.001	\\
		cross entropy, validation	&	0.774	&	0.944	&	1.240	&	0.986	&	0.236	\\
		\midrule											
		time needed	[s] &	34.0	&	32.0	&	31.0	&	32.3	&	1.528	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of MobileNet V2 with original test-data.]{Measurements of three training runs of MobileNet V2
		with the original test-data, taken from log output with tensorboard.}\label{tab:m_o}
\end{table}

\tocless\subsection{Imagenet Data}
For the results in figure~\ref{tab:m_i}, 5.187 images are used.
Of those 648 belong to
category foyer, 721 to hall, 1.029 to meetingroom, 867 to operating theatre, 1.236 to 
outside and 686 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.890	&	0.890	&	0.850	&	0.877	&	0.023	\\
		validation accuracy	&	0.670	&	0.680	&	0.620	&	0.657	&	0.032	\\
		\midrule											
		cross entropy, training	&	0.288	&	0.261	&	0.365	&	0.305	&	0.054	\\
		cross entropy, validation	&	1.444	&	1.282	&	1.503	&	1.410	&	0.114	\\
		\midrule											
		time needed [s]	&	315.0	&	329.0	&	317.0	&	320.333	&	7.572	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of MobileNet V2 with Imagenet test-data.]{
		Measurements of three training runs of MobileNet V2
		with the Imagenet data, taken from log output with tensorboard.}\label{tab:m_i}
\end{table}

\tocless\subsection{Flickr Data}
For the results in figure~\ref{tab:m_f}, 7.864 images are used.
Of those 1282 belong to corridor, 1112 to
category foyer, 1104 to hall,  1052 to meetingroom, 1136 to operating theatre, 1120 to 
outside and 1058 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.980	&	0.960	&	0.950	&	0.963	&	0.015	\\
		validation accuracy	&	0.540	&	0.530	&	0.470	&	0.513	&	0.038	\\
		\midrule											
		cross entropy, training	&	0.238	&	0.244	&	0.237	&	0.240	&	0.004	\\
		cross entropy, validation	&	2.069	&	1.889	&	2.571	&	2.176	&	0.353	\\
		\midrule											
		time needed [s]	&	315.0	&	323.0	&	322.0	&	320.0	&	4.359	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of MobileNet V2 with Flickr test-data.]{
		Measurements of three training runs of MobileNet V2
		with the Flickr data, taken from log output with tensorboard.}\label{tab:m_f}
\end{table}

\section{NASNet Mobile}
\tocless\subsection{Original Data}
For the results in figure~\ref{tab:nm_o}, 214 images are used. Of those 23 belong to
category foyer, 21 to hall, 50 to corridor, 25 to meetingroom, 23 to operating theatre, 41 to 
outside and 31 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	1.000	&	1.000	&	1.000	&	1.000	&	0	\\
		validation accuracy	&	0.690	&	0.700	&	0.760	&	0.717	&	0.038	\\
		\midrule											
		cross entropy, training	&	0.012	&	0.012	&	0.009	&	0.011	&	0.002	\\
		cross entropy, validation	&	1.710	&	1.638	&	1.379	&	1.576	&	0.174	\\
		\midrule											
		time needed [s]	&	34.0	&	33.0	&	34.0	&	33.7 &	0.577	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of NASNet Mobile with original test-data.]{Measurements of three training 
		runs of NASNet Mobile with the original test-data, taken from log output with tensorboard.}\label{tab:nm_o}
\end{table}

\tocless\subsection{Imagenet Data}
For the results in figure~\ref{tab:nm_i}, 5.187 images are used.
Of those 648 belong to
category foyer, 721 to hall, 1.029 to meetingroom, 867 to operating theatre, 1.236 to 
outside and 686 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.830	&	0.790	&	0.740	&	0.787	&	0.045	\\
		validation accuracy	&	0.750	&	0.690	&	0.670	&	0.703	&	0.042	\\
		\midrule											
		cross entropy, training	&	0.399	&	0.560	&	0.586	&	0.515	&	0.101	\\
		cross entropy, validation	&	0.817	&	1.083	&	1.114	&	1.005	&	0.163	\\
		\midrule											
		time needed [s]	&	328.0	&	324.0	&	337.0	&	329.667	&	6.658	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of NASNet Mobile with Imagenet test-data.]{Measurements of three training runs of NASNet Mobile
		with the Imagenet test-data, taken from log output with tensorboard.}\label{tab:nm_i}
\end{table}

\tocless\subsection{Flickr Data}
For the results in figure~\ref{tab:nm_f}, 7.864 images are used.
Of those 1282 belong to corridor, 1112 to
category foyer, 1104 to hall,  1052 to meetingroom, 1136 to operating theatre, 1120 to 
outside and 1058 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.860	&	0.780	&	0.850	&	0.830	&	0.044	\\
		validation accuracy	&	0.600	&	0.540	&	0.530	&	0.557	&	0.038	\\
		\midrule											
		cross entropy, training	&	0.440	&	0.522	&	0.450	&	0.471	&	0.045	\\
		cross entropy, validation	&	1.200	&	1.332	&	1.304	&	1.279	&	0.070	\\
		\midrule											
		time needed [s]	&	330.0	&	334.0	&	327.0	&	330.333	&	3.512	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of NASNet Mobile with Flickr test-data.]{Measurements of three training runs of NASNet Mobile
		with the Flickr test-data, taken from log output with tensorboard.}\label{tab:nm_f}
\end{table}

\section{NASNet Large}
\tocless\subsection{Original Data}
For the results in figure~\ref{tab:nl_o}, 214 images are used. Of those 23 belong to
category foyer, 21 to hall, 50 to corridor, 25 to meetingroom, 23 to operating theatre, 41 to 
outside and 31 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	1.000	&	1.000	&	1.000	&	1.000	&	0	\\
		validation accuracy	&	0.740	&	0.780	&	0.780	&	0.767	&	0.023	\\
		\midrule											
		cross entropy, training	&	0.014	&	0.017	&	0.009	&	0.014	&	0.004	\\
		cross entropy, validation	&	0.894	&	1.051	&	0.927	&	0.957	&	0.083	\\
		\midrule										
		time needed [s]	&	36.0	&	34.0	&	34.0	&	34.7	&	1.155	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of NASNet Large with original test-data.]{Measurements of three training runs of NASNet Large
		with the original test-data, taken from log output with tensorboard.}\label{tab:nl_o}
\end{table}

\tocless\subsection{Imagenet Data}
For the results in figure~\ref{tab:nl_i}, 5.187 images are used.
Of those 648 belong to
category foyer, 721 to hall, 1.029 to meetingroom, 867 to operating theatre, 1.236 to 
outside and 686 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.840	&	0.870	&	0.840	&	0.850	&	0.017	\\
		validation accuracy	&	0.770	&	0.740	&	0.710	&	0.740	&	0.030	\\
		\midrule											
		cross entropy, training	&	0.362	&	0.396	&	0.374	&	0.377	&	0.017	\\
		cross entropy, validation	&	0.708	&	0.943	&	1.053	&	0.901	&	0.176	\\
		\midrule											
		time needed [s]	&	325.0	&	330.0	&	319.0	&	324.667	&	5.508	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of NASNet Large with Imagenet test-data.]{Measurements of three training runs of NASNet Large 
		with the Imagenet test-data, taken from log output with tensorboard.}\label{tab:nl_i}
\end{table}

\tocless\subsection{Flickr Data}
For the results in figure~\ref{tab:nl_f}, 7.864 images are used.
Of those 1282 belong to corridor, 1112 to
category foyer, 1104 to hall,  1052 to meetingroom, 1136 to operating theatre, 1120 to 
outside and 1058 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.910	&	0.890	&	0.880	&	0.893	&	0.015	\\
		validation accuracy	&	0.630	&	0.670	&	0.550	&	0.617	&	0.061	\\
		\midrule											
		cross entropy, training	&	0.298	&	0.313	&	0.373	&	0.328	&	0.040	\\
		cross entropy, validation	&	1.183	&	1.036	&	1.209	&	1.143	&	0.093	\\
		\midrule											
		time needed [s]	&	329.0	&	324.0	&	324.0	&	325.667	&	2.887	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of NASNet Large with Flickr test-data.]{Measurements of three training runs of NASNet Large 
		with the Flickr test-data, taken from log output with tensorboard.}\label{tab:nl_f}
\end{table}

\section{PNASNet}
\tocless\subsection{Original Data}
For the results in figure~\ref{tab:p_o}, 214 images are used. Of those 23 belong to
category foyer, 21 to hall, 50 to corridor, 25 to meetingroom, 23 to operating theatre, 41 to 
outside and 31 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	1.000	&	1.000	&	1.000	&	1.000	&	0	\\
		validation accuracy	&	0.900	&	0.870	&	0.870	&	0.880	&	0.017	\\
		\midrule											
		cross entropy, training	&	0.027	&	0.019	&	0.027	&	0.024	&	0.005	\\
		cross entropy, validation	&	0.668	&	0.719	&	0.763	&	0.717	&	0.048	\\
		\midrule											
		time needed [s]	&	35.0	&	35.0	&	34.0	&	34.7	&	0.577	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of PNASNet with original test-data.]{Measurements of three training runs of PNASnet with
		the original test-data, taken from log output with tensorboard.}\label{tab:p_o}
\end{table}

\tocless\subsection{Imagenet Data}
For the results in figure~\ref{tab:p_i}, 5.187 images are used.
Of those 648 belong to
category foyer, 721 to hall, 1.029 to meetingroom, 867 to operating theatre, 1.236 to 
outside and 686 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.870	&	0.840	&	0.870	&	0.860	&	0.017	\\
		validation accuracy	&	0.790	&	0.880	&	0.840	&	0.837	&	0.045	\\
		\midrule											
		cross entropy, training	&	0.329	&	0.408	&	0.367	&	0.368	&	0.040	\\
		cross entropy, validation	&	1.001	&	0.456	&	0.587	&	0.681	&	0.284	\\
		\midrule											
		time needed [s]	&	324.0	&	321.0	&	327.0	&	324.000	&	3.000	\\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of PNASNet with Imagenet test-data.]{Measurements of three training runs of PNASNet 
		with the Imagenet test-data, taken from log output with tensorboard.}\label{tab:p_i}
\end{table}

\tocless\subsection{Flickr Data}
For the results in figure~\ref{tab:p_f}, 7.864 images are used.
Of those 1282 belong to corridor, 1112 to
category foyer, 1104 to hall,  1052 to meetingroom, 1136 to operating theatre, 1120 to 
outside and 1058 to restaurant.

\begin{table}[H]
	\centering
	\begin{tabularx}{.8\textwidth}{r c c c c l }
		\toprule											
		Measured	&	run 1	&	run 2	&	run 3	&	$\varnothing$	&	$\sigma$	\\
		\midrule											
		training accuracy	&	0.920	&	0.910	&	0.850	&	0.893	&	0.038	\\
		validation accuracy	&	0.730	&	0.660	&	0.690	&	0.693	&	0.035	\\
		\midrule											
		cross entropy, training	&	0.314	&	0.310	&	0.442	&	0.355	&	0.075	\\
		cross entropy, validation	&	0.841	&	1.354	&	1.054	&	1.083	&	0.258	\\
		\midrule											
		time needed [s]	&	329.0	&	321.0	&	323.0	&	324.333	&	4.163 \\
		\bottomrule											
	\end{tabularx}
	\caption[Measurements of PNASNet with Flickr test-data.]{Measurements of three training runs of PNASNet
		with the Flickr test-data, taken from log output with tensorboard.}\label{tab:p_f}
\end{table}

\section{Comparison}

\tocless\subsection{Original Data}
All values in figure~\ref{tab:comp_o} are the mean average of three runs on the original data.

\begin{table}[H]
	\noindent\centering
	\begin{tabular}{r c c c c c}
		\toprule
		\multirow{2}{*}{Network} & \multicolumn{2}{c}{accuracy} &
		\multicolumn{2}{c}{cross entropy} & \multirow{2}{*}{time [s]}\\
		& training & validation & training & validation & \\
		\midrule											
		Inception V3  & 1.000 & 0.717 & 0.009 & 0.814 & 31.3 \\
		MobileNet V2  & 1.000 & 0.700 & 0.005 & 0.986 & 32.3 \\
		NASNet Mobile & 1.000 & 0.717 & 0.011 & 1.576 & 33.7 \\
		NASNet Large  & 1.000 & 0.767 & 0.014 & 0.957 & 34.7 \\
		PNASNet       & 1.000 & 0.880 & 0.024 & 0.717 & 34.7 \\
		\bottomrule											
	\end{tabular}
	\caption[Comparison of training measurements with original data.]{
		Comparison of training measurements for the original data.
	}\label{tab:comp_o}
\end{table}

\tocless\subsection{Imagenet Data}
All values in figure~\ref{tab:comp_i} are the mean average of three runs on the Imagenet data.

\begin{table}[H]
	\noindent\centering
	\begin{tabular}{r c c c c c}
		\toprule
		\multirow{2}{*}{Network} & \multicolumn{2}{c}{accuracy} &
		\multicolumn{2}{c}{cross entropy} & \multirow{2}{*}{time [s]}\\
		& training & validation & training & validation & \\
		\midrule											
		Inception V3  & 0.817 & 0.707 & 0.462 & 0.796 & 321 \\
		MobileNet V2  & 0.877 & 0.657 & 0.305 & 1.410 & 320 \\
		NASNet Mobile & 0.787 & 0.703 & 0.515 & 1.005 & 330 \\
		NASNet Large  & 0.840 & 0.770 & 0.377 & 0.901 & 325 \\
		PNASNet       & 0.870 & 0.837 & 0.368 & 0.681 & 324 \\
		\bottomrule											
	\end{tabular}
	\caption[Comparison of training measurements with Imagenet data.]{
		Comparison of training measurements for the Imagenet data.
	}\label{tab:comp_i}
\end{table}

\tocless\subsection{Flickr Data}
All values in figure~\ref{tab:comp_f} are the mean average of three runs on the Flickr data.

\begin{table}[H]
	\noindent\centering
	\begin{tabular}{r c c c c c}
		\toprule
		\multirow{2}{*}{Network} & \multicolumn{2}{c}{accuracy} &
		\multicolumn{2}{c}{cross entropy} & \multirow{2}{*}{time [s]}\\
		& training & validation & training & validation & \\
		\midrule											
		Inception V3  & 0.917 & 0.587 & 0.322 & 1.490 & 322 \\
		MobileNet V2  & 0.963 & 0.513 & 0.240 & 2.176 & 320 \\
		NASNet Mobile & 0.830 & 0.557 & 0.471 & 1.279 & 330 \\
		NASNet Large  & 0.893 & 0.617 & 0.328 & 1.143 & 326 \\
		PNASNet       & 0.893 & 0.693 & 0.355 & 1.083 & 324 \\
		\bottomrule											
	\end{tabular}
	\caption[Comparison of training measurements for the Flickr data.]{
		Comparison of training measurements for the Flickr data.
	}\label{tab:comp_f}
\end{table}

\subsection{Overview}
In figure~\ref{fig:timecomp} the time need to complete the test-runs can be 
seen. In figure~\ref{fig:finalperf} the final performance of each network side by side
is displayed.

\begin{figure}[H]
	\centering
	{\noindent\includegraphics[width=\textwidth]{../images/results/time_needed.png}}
	\caption[Time needed for the test-runs.]
	{Time needed for the test-runs.}\label{fig:timecomp}
\end{figure}

\begin{figure}[H]
	\centering
	{\noindent\includegraphics[width=\textwidth]{../images/results/fina_perf.png}}
	\caption[Final performance of each network side by side.] 
	{Final performance of each network side by side.}\label{fig:finalperf}
\end{figure}
