\chapter{Basics}\label{cha:basics}

\section{Content Management Systems}\label{sec:cms}
A system designed to manage the content of a website or other electronic
resource that is used collaboratively by a number of people~\cite{oxforddict}.

\par

Such a system should make it very easy for a user to create and maintain websites.
It automatically pulls content and shows appropriate pages, and thus spares the
trouble of updating everything manually~\cite{Wakode}.

\section{Image Tagging}\label{sec:imagetagging}
Image tagging means to assign words to images to describe their content~\cite{autimg}.

\par

This technique is very useful when the images should be classified somehow, as
the need for a solution for automatically assigning those tags to images to make it easier
to group them.
For example it enables to search for a group of images concerning a topic by text.
Furthermore, it makes it possible to group images by categories or content, which
is essential to provide a structured and clear view of available products for instance.
In a highly competitive field like online shops or similar fields this is very important,
as a user can change to a competitor by a mouse-click.

\section{Artificial Intelligence}\label{sec:ai}
The term stands for man-made intelligence~\cite{artdef}.
Google trends show
that the interest in this topic considering web searches is growing steadily
even today. There
are several ways on how to create artificial intelligence, but in this thesis
neural networks are used, which are described in the following chapters.

\subsection{Machine Learning}
How is it, that an artificial network is capable of learning for itself? There are several possibilities
on how to accomplish this, but only Supervised Learning is discussed in this thesis.

\subsubsection{Supervised Learning}
In supervised learning, test-data is presented sample by sample and if the network
produces the correct output, the weights and biases of the network stay untouched.
If the output is not satisfying, the weights are adapted in order
to make the network classify the images correctly.
The test-data is split into three sets. One is called the training set, it is used
to adapt the weights of the network. The other sets are for testing and validation,
with the first one the network performance is measured and with the second one
the final performance is validated and best approach picked as the trained network.
The Network is trained in epochs in most cases.
One epoch represents one entire cycle through the available test-data.
The number of epochs follows no conventions and can be picked by need. After the defined
number of epochs or the required performance is reached, learning is considered finished.
This type of learning is easy to implement and picked to train the network for image classification
in this thesis~\cite{annp2017}.

\subsubsection{Backpropagation}
As already mentioned, supervised learning works by adapting the weights of the neurons connected to the output.
This is working just fine for a perceptron network consisting only of one layer,
but when it comes to more complex types of networks this is not sufficient. A method to
train multilayer networks is called backpropagation. Basically, this means that the error of the network
is backpropagated to the input, so that every neuron is affected and adopted to the corresponding output. The
principle is the same, if the network produces the wrong output on a sample, the weights of the last layer are
changed. This rate of change is dynamic, more on this later on. Once the weights of the layer are adjusted, the
same thing is done for the next layer until the input layer is reached. 
Wrapped up, this means that the network is optimized for a specific task by 
using an error function~\cite{backprop}.

\subsubsection{Measuring the performance of a network}
To decide whether a network is trained or working well, it is necessary to measure
the performance somehow. In supervised learning this can be done by executing the network
on all the test samples. Thus, the calculation is easy by just dividing the number of successfully
classified samples by the total number of samples.

\par

While training, other more advanced methods are used. Because just measuring if
the test-data is correctly classified or not can result in problems as shown in figure~\ref{fig:prob}.
To further increase training effectivity, instead of the simple method mentioned previously some kind of
error minimization can be used. This means to not only measure how many of the samples were
correctly classified, but also how confident the network chose its decision. To illustrate,
a small example will we explained.

\begin{figure}
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Incorrectly classified sample.]
		{\noindent\includegraphics[width=0.85\textwidth]{../images/problem_lin.png}}
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Correctly classified sample.]
		{\noindent\includegraphics[width=0.85\textwidth]{../images/solution_lin.png}}
	\end{minipage}%
	\caption[Linear separation problem.]{
		In (a) the sample is incorrectly classified whereas in (b) it is correctly
		classified~\cite{annp2017}.
	}\label{fig:prob}
\end{figure}

Let's assume we have a network where in the output layer there is a single neuron with a linear activation function.
This means this network can produce a decimal output between 0 and 1. Let's say we want to classify the data
in two groups represented by A and B, where the sample belongs to group B if the output greater or equal to 0.5 and to
group A if it is smaller than 0.5.
If we just note if the group is correct we actually loose some information. Because if the network calculates an
activation potential of 0.7 for instance, we know that the network calculated that the sample belongs to group B, but
also has similarities to group A. This means an error of 0.3. If we sum up those errors for every sample, we can tell how confident
the network is on the set. If we then minimize this error while training, the marked sample in figure~\ref{fig:prob} shows
that it is correctly classified. Often used methods are mean squared error or 
cross entropy, which means the error between two probability distributions~\cite{pandey}.

\begin{figure}[H]
\end{figure}

%\subsubsection{Cross Entropy}


\subsection{Neural Networks}\label{sec:nn}
Initial ideas for artificial neural networks are inspired by the human brain~\cite{annp2017}.

\par

But even today the human brain provides a lot of help when developing new architectures. A human brain
is made from billions of cells called neurons. They are connected to each other
by synapses and work by electric impulses. The huge net of these neurons
is making us humans intelligent and capable of learning new things~\cite{kaur2012}.

\par

This learning possibility is the main potential of this technology. As it enables
to implement solutions for all kinds of problems without the need to describe
everything algorithmically. The only things needed is an appropriate network, where
appropriate means a network with necessary complexity and test-data. A very simple neural network can't
describe an XOR function~\cite{annp2017}, thus utilizing such a network for image classification
won't produce any acceptable result. The other way round, meaning a network with too high
complexity, can also cause trouble. Aside the higher need in computational power,
there is also the risk of overfitting, which is discussed later on.


\par 

Artificial neural networks consist of \(1-n\) neurons
which are connected to each other by weights and in layers, where
on the rightmost side the network takes the data as input, and
on leftmost side the user defined output, which can be classes, numbers
or anything else. In this case the output are the tags, in which
the images are separated into.

\par

The calculus behind neural networks is rather simple if viewed in layers.
The decision if a neuron is activated is made by calculating the activation potential
with the formula~\ref{e:sum}.
Where \(x_{i}\) represents the input value and \(w_{i}\) the weight with which the
incoming value, which can either be an input or the output from another neuron, is connected to the neuron.
At last \(b\) represents the bias. This formula is executed and the result is fed into the activation function.
Popular and widely used functions are the step function, linear function, Gaussian function
and many more.
If this is executed layer by layer starting at the input layer, neuron by neuron 
the output will be one of the outputs defined beforehand~\cite{basics}.

\begin{equation}\label{e:sum}
	\sum\limits_{i=1}^n x_{i} * w_{i} - b
\end{equation}


\par

This is the simplest form of a neural network. There are lots of more advanced ways
to build one. In the field of speech recognition for instance, networks with some kind of short and
longterm memory are used, to mimic the way the human brain processes speech. For computer
vision on the other hand, convolutional networks have been established. This means that not
every neuron is connected to the next layer ones. Instead, the imaginary images consisting
of pixels is split into small clusters, typically consisting of $3\times3$ or $5\times5$ pixels
(or neurons).

\par

A question may be how to feed images in such a network only understanding numbers? The answer is
to use a numerical value. Pixels in an image for example do have a value for each color in the RGB space,
where RGB stands for red green blue, which is a common way to display color or any
other color space.

\par

Each color space has it's on advantages and disadvantages. RGB for example has the
disadvantage that the model produces a non linear and discontinuous space and thus makes
changes in color hard to follow. This issue does not exist when using the HSL colorspace,
which stands for Hue Saturation Lightness. Though this model introduces an other issue,
namely that gray color can not be represented as the value is undefined if all three of the
RGB values are equal~\cite{ascml2018}. Only the RGB color space is used in 
this thesis.  

\par

This value then can be used to be
fed into the network. In speech recognition it could be the
frequency spectrum of the sound. So to make it possible
to work with information in a network is by expressing it with numbers, which is already done when using
digital information.

\par

To make it easier to keep track of all the different network types some basic architectures
have established. Some well-known and widely used types are described further on.

\subsubsection{Network Types}
The simplest network is called perceptron and consists of only one neuron. Because of this
simplicity this network can only output one value which can be zero or one if used for linear separation
for instance. Another one to mention is the multilayer perceptron network. This one is built from several
layers of neurons and because of the higher amount of neurons able to learn more complex things~\cite{annp2017}.

\subsubsection{Overfitting}
If a network consists of too many neurons, it can happen that it completely
learns the test-data in such a way that it looses its generalization capability. This means
in the learning phase it is able to predict the test-data with a very high success-rate.
But when unknown data is fed into the network, the performance is far from that.
One counter-measure is to further split up the test-data to get a validation set. This set
is not used for learning but for determining the performance during learning, to make
visible if the network tends to over-fit~\cite{annp2017}. To give an example,
in figure~\ref{img:overfit} (a)
the output of an overfitting MLP, standing for MultiLayer Perceptron, which is trained on measurements of a sine
wave is shown. In figure~\ref{img:overfit} (b) an MLP which is not overfitting is displayed.

\begin{figure}
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Overfitting MLP.]{\includegraphics[width=0.8\textwidth]
		{../images/overfitting.png}}
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Not overfitting MLP.]{\includegraphics[width=0.8\textwidth]
		{../images/overfitting_fixed.png}}
	\end{minipage}%
	\caption[Overfitting and not overfitting MLP comparison.]{A MLP
		which was trained on the measurements of a sine wave,
		overfitting in (a) and not overfitting in (b)~\cite{annp2017}.
	}\label{img:overfit}
\end{figure}

\subsection{Data Normalization}
When using a neural network, the number of input neurons is fixed and the data type
is a decimal number in most cases. Thus, the data presented to the network has to be
prepared and scaled in order to fit those requirements~\cite{datanorm}. This process is called data
normalization. If you take a network of which one input is the speed of something in
meters per second, feeding a measurement in yards per second would most likely not
produce the wanted output, as the network does not know which data-type the input has.
In this use case the test data consists of images which are stored in the well-known
and widely used image format jpeg~\footnote{\url{https://www.w3.org/Graphics/JPEG/}}.
This is a data-type which is very space efficient and suitable
to deploy via the web. To make these images ready to be put into the network several
steps are necessary.
\subsubsection{Resolution}
The first step is to adapt the image to the required resolution.
This resolution is defined by the number of input neurons of the network used.
An image with a resolution of 12 times 12, which equals 144 pixel values, can't be put
into a network with 100 neurons in the input layer. This results in the question of how to decide resolution.
There is no correct answer to this question
it depends on the use case and the only thing which someone can do up to now is to test
and find the best settings and design by trial and error. The main point to be considered for
this decision is the amount of detail necessary for the network to work properly. The more detail the more the network can see,
the more patterns it can find. But if most of the details are not needed this would
result in a lot of undesired noise, when the overall amount of neurons stays the same.
If you build a network to decide if the input data is a carrot or a tomato, it is not
necessary for the network to see if a tomato or a carrot has a little of dirt on it
or little scratches, rather the shape and the colors are relevant. But if the scratches are important,
when checking for the quality of a tomato for instance, the resolution has to be high enough, so the
network can see them~\cite{medres}. Thus, the data should
be normalized in order not to present too much information to the network. Not only
the amount of detail should be considered when deciding the amount of input neurons, but
also the needed network performance and maximum network complexity has to be considered.
If the trained network is deployed in form of a hardware which is optimized for the
network this might not be a big deal, but if the network is using online learning and is
executed on a productive server on which not only the network is executed or the network
is frequently used, or used in parallel, more attention should be paid on the complexity in order
to satisfy the required performance.

\subsubsection{Chromaticity}
As it is with resolution and details, the same thing holds for chromaticity. If the detail of color
is not needed, then it should be removed, as it is easier for the network to adapt to the
information, because less information results in less distraction provided no really needed
information is missing. When classifying tomatoes for their quality in 
example, the gray color values are enough to get acceptable results~\cite{annp2017}.  

\section{Available Solutions}

\subsection{Google}\label{sec:google}
Google  provides a solution for image recognition.
This tool is able to automatically assign tags to any images it is getting presented, but possibilities
go far further from that. The so-called Vision API, which is an application programmable interface for image recognition,
is also able to find objects in images and provides the coordinates of the objects found. This functionality
is not relevant for this thesis, thus no attention will be paid on this. When providing images from the test-data
used to train the networks in this example the tags sometimes fit just fine, but sometimes tags have the wrong confidence. 
Examlpes can be seen in the figures~\ref{img:google_correct1},~\ref{img:google_correct2} and~\ref{img:google_correct3}.
\newline

\begin{figure}
\noindent\includegraphics[width=\textwidth]{../images/vision_demo.png}
\caption[Vision API, classifying a conference hall image.]{The Vision API correctly assigns the desired tag even at highest probability.
	But when presenting more unusual images as
	an image from a building outside, where the tag should be something similar to outside the network doesn't find
	the corresponding tag.}\label{img:google_correct1}
\end{figure}

\begin{figure}
\noindent\includegraphics[width=\textwidth]{../images/vision_corridor.png}
\caption[Vision API, classifying a corridor image.]{Sometimes Vision API correctly assigns
	the desired tag, but not the correct probability. Also,
	a lot of undesired tags are assigned, which would have to be filtered once 
	again.}\label{img:google_correct2}
\end{figure}

\begin{figure}
\noindent\includegraphics[width=\textwidth]{../images/vision_outside.png}
\caption[Vision API, classifying an outside image.]{The image of a building 
	outside presented to the Vision API.}\label{img:google_correct3}
\end{figure}

\subsection{Amazon}\label{sec:amazon}
As Google Amazon also provides a standard tagging API which can be tested free of charge.

\par

The output on the images
are nearly the same as Google gives back, and this results in the same problem. It indeed finds the desired tag most
of the time. Examples can be seen in figures~\ref{img:amazon_correct1},~\ref{img:amazon_correct2} and~\ref{img:amazon_correct3},
but they would have to be filtered. And as the tags are defined by the user it could be that the network
does not know about the existence of the desired tag.
\newline

\begin{figure}
\noindent\includegraphics[width=\textwidth]{../images/amazon_meetingroom.png}
\caption[Amazon API, classifying a conference hall image.]{Amazon API is also capable of finding objects and marking
	them in the presented image.}\label{img:amazon_correct1}
\end{figure}

\begin{figure}
\noindent\includegraphics[width=\textwidth]{../images/amazon_outside.png}
\caption[Amazon API, classifying an outside image.]{Amazon API, classifying an 
	outside image.}\label{img:amazon_correct2}
\end{figure}

\begin{figure}
\noindent\includegraphics[width=\textwidth]{../images/amazon_corridor.png}
\caption[Amazon API, correct classification.]{In this image the API tags the correct tag with the highest confidence.}\label{img:amazon_correct3}
\end{figure}

