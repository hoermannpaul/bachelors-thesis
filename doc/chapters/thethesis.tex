\chapter{Fully Automated Image Classification
	utilizing Neural Network Architectures
	trained on Tagged Data Sets}\chaptermark{
	Fully Automated Image Classification
	utilizing Neural Network Architectures
}\label{cha:thesis}

\section{Test Data}

\subsection{Amount of Test Data}
As already mentioned, a neural network is generalizing from the data it learns from.
Thus, the initial believe may be that the more data available for training, the better
the network is performing. But this does not hold true as the network potentially looses
its generalization capability~\cite{largeBatch}. With the amount of test-data used in this thesis
this should not be the case, as the amount of test-data is very limited, and not the complete network
but only a new last layer is trained.

\subsection{Sources}
To gain enough test-data, three sources are used. As mentioned previously, those
sources consist of flickr.com, image-net.org and elements. Judging by quality of the test-data,
the images provided by elements are by far the best, with a resolution of at least $1000\times1000$.
Those images do also represent the real world data on which the network should later be used on.
The image quality of the other two sources vary a lot, but are much worse in general. The resolution
of those images are a lot lower, sometimes featuring highly upscaled images. When upscaling an image,
it's resolution is increased by maintaining the aspect ratio, for which several techniques are available,
but not discussed in this thesis.
Image size is also very individual, the original images do
have several megabytes in size, whereas some images of the downloaded ones have less than 100 kilobytes.

\subsection{Original Data}
The images are divided into 7 categories.
Building outside, operating theater, corridor, auditorium, restaurant, meeting room and foyer.
In figure~\ref{fig:original_samples} one can see an image from each category.


\begin{figure}[H]
	\begin{minipage}{0.5\textwidth}
		\subfloat[Image of a building outside.]
		{\includegraphics[width=0.8\textwidth]{../images/original/outside.jpg}}
		\centering
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of an operating theater.]
		{\includegraphics[width=0.8\textwidth]{../images/original/op.jpg}}
	\end{minipage}%
	\newline
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of a corridor.]
		{\includegraphics[width=0.8\textwidth]{../images/original/corridor.jpg}}
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of an auditorium.]
		{\includegraphics[width=0.8\textwidth]{../images/original/hall.jpg}}
	\end{minipage}%
	\newline
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of a restaurant.]
		{\includegraphics[width=0.8\textwidth]{../images/original/restaurant.jpg}}
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of a meeting room.]
		{\includegraphics[width=0.8\textwidth]{../images/original/meeting.jpg}}
	\end{minipage}%
	\newline
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of a foyer.]
		{\includegraphics[width=0.8\textwidth]{../images/original/foyer.jpg}}
	\end{minipage}
	\caption[Data samples from original test-data.]
	{An image for each category,
		all from the original test-data.}\label{fig:original_samples}
\end{figure}
\subsection{Flickr}
The data from flickr is downloaded by a script implemented for this thesis which
gets the data by search term. For most of the images this
works fine as shown in figure~\ref{fig:flickr_good},
but some images do not fit the search term at
all and thus have to be cleaned before using it as seen in figure~\ref{fig:flickr_bad}.

\begin{figure}[H]
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of a restaurant.]
		{\includegraphics[width=0.8\textwidth]{../images/flickr_good/restaurant.jpg}}
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of a meeting room.]
		{\includegraphics[width=0.8\textwidth]{../images/flickr_good/meeting.jpg}}
	\end{minipage}%
	\caption[Images downloaded from flickr which fit the needed type.]
	{Most of the time the images fit the desired type.}\label{fig:flickr_good}
\end{figure}

\begin{figure}[H]
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of a corridor.]
		{\includegraphics[width=0.8\textwidth]{../images/flickr_bad/corridor.jpg}}
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\subfloat[Image of an auditorium.]
		{\includegraphics[scale=0.135]{../images/flickr_bad/auditorium.jpg}}
	\end{minipage}%
	\caption[Images downloaded from flickr which do not fit.]{Some images do not
		fit the desired type at all.}\label{fig:flickr_bad}
\end{figure}

\section{Preprocessing}
As the resolution is fixed for the network types tested in this thesis
there is no possibility to test performance with different resolutions.

\section{Network Models}
On tensorflow hub there are several pre-trained networks available.  Networks
of different architectures and different complexity.  To give an overview the following
five where selected for testing in this thesis.  

\begin{itemize}

	\item \textbf{Inception V3}
The Inception V3 network is, as the name suggests, an adaption of the Inception V2 network.
The main point of the Inception architecture is to scale up networks by factorizing convolutions
and aggressive regularization. This means that the computational cost of the network is reduced by only loosing
very little to no performance~\cite{inception}.

	\item \textbf{MobileNet V2}
Focus of this network is maximizing performance by keeping in mind
that it is running on mobile device~\cite{mobilenet}.

	\item \textbf{NASNet Large}
The idea behind NASNet is, to have a Recurrent Neural Network as Controller, which is searching for better structures
by training and learning from the results\cite{nasnet1}.

	\item \textbf{NASNet Mobile}
This is a variant of the NASNet Variant suitable for devices with limited power, as smart phones
for example. It is utilizing some of the architectural specialities of
NASNet Large by reducing performance cost.

	\item \textbf{PNASNet}
The PNASNet is the computational most intensive network compared in this 
thesis. The way it works is similar to the way how NASNet models are trained.
The network is improving the network's structure over time to improve 
performance over time~\cite{pnasnet}.

\end{itemize}

\section{Training Performance on CPU vs GPU}
As the key mathematical calculations done for training and executing are vector and
matrix multiplications, there is a drastic difference in performance when comparing CPU 
(Central Processing Unit) and GPU (Graphic Processing Unit)
execution. Vector and matrix-multiplications can be easliy parallelized, thus the algorithms take
profit of the high core count of a GPU, whereas a CPU is limited to typically 2 to 8 cores,
even high performance server CPUs don't provide more cores than 16 to 32 at most. This number is
little compared to a typical 1000 or more cores on a GPU nowadays. To see if this also improves performance
when only retraining, tests are executed and presented in a later chapter.

\section{Training Progress}
In figure~\ref{img:pnetprog} the progress of the training can be seen. From that figure it is
easily visible that the performance reaches its maximum very quickly, suggesting that too few
test-data is used. The training process took 35 seconds.

\begin{figure}[H]
	\centering
	\includegraphics[width=.75\textwidth]{../images/trainprog.png}
	\caption[Training progress of Inception V3.]{Training progress of Inception V3.}\label{img:pnetprog}
\end{figure}

