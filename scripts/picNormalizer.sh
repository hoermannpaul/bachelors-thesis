#!/bin/bash

ls $(pwd)/*.JPG 2>/dev/null 1>/dev/null

if [ $? -gt 0 ]; then

	echo "no files to rename!";

else

	for filename in $(pwd)/*.JPG
	do

		extension="${filename##*.}"
		basename="${filename%.*}"
		echo "mv $filename $basename.jpg"
		mv $filename "$basename.jpg"

	done

fi

ls $(pwd)/*.jpg 2>/dev/null 1>/dev/null

if [ $? -gt 0 ]; then

	echo "no files to convert!";

else

	rm $(pwd)/*.normalized* 2>/dev/null || true

	for filename in $(pwd)/*.jpg
	do

		extension="${filename##*.}"
		basename="${filename%.*}"
		echo "gm convert -resize "\"$1!\"" $filename $basename.normalized.jpg"
		gm convert -resize $1! $filename "$basename.normalized.jpg"

	done

fi
