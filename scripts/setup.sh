#!/bin/bash

mkdir -p /var/www/pimcore/tensorflow/cache
mkdir -p /var/www/pimcore/tensorflow/models
mkdir -p /var/www/pimcore/tensorflow/labels

cd /var/www/pimcore/tensorflow
curl https://bitbucket.org/hoermannpaul/bachelors-thesis/raw/62bafa6fc8884e3b41e9c79d6cf510d2089e743e/python/prototype.0.1/retrain.py > retrain.py
curl https://bitbucket.org/hoermannpaul/bachelors-thesis/raw/62bafa6fc8884e3b41e9c79d6cf510d2089e743e/python/prototype.0.1/label_image.py > label_image.py
