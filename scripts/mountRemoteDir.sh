#!/bin/bash

sshfs -o allow_other -p 8001 pimcore_admin@localhost:/var/www/pimcore /media/remote
sshfs -o allow_other -p 8001 pimcore_admin@localhost:/home/pimcore_admin /media/remote_home

