#encoding: UTF-8 

import sys
import random
import tensorflow as tf
from os import listdir
from os.path import isfile, join

class namedImage(object):
    name = None
    image = None
    def __init__(self, name, tf.image):
        self.name = name
        self.image = image

class dataSet(object):
    trainingData = None
    testData = None
    def __init__(self, trainingData, testData):
        self.trainingData = trainingData
        self.testData = testData

def readDataToTuples(dir):
    toReturn = {}
    currentDir = None
    for directory in listdir(dir):
        if not isfile(join(dir, directory)):
            toReturn[directory] = []
            for image in listdir(join(dir, directory)):
                toReturn[directory].append(namedImage(image, tf.image.resize_images(tf.image.decode_jpeg(image), [1000, 800])))
            if len(toReturn[directory]) == 0:
                del toReturn[directory]
    return toReturn

def splitSetIntoTrainingAndTest(data):
    trainingData = {}
    testData = {}
    for key in data:
        if len(data[key]) == 1:
            sys.exit(2)
        amount = int((len(data[key]))*0.1) + 1
        trainingData[key] = []
        testData[key] = []
        for image in data[key]:
            testData[key].append(data[key][random.randrange(len(data[key]) - 1)])
            if amount == 1:
                break
            amount -= 1
        for image in data[key]:
            trainingData[key].append(image)
    return dataSet(trainingData, testData)

if len(sys.argv) < 2:
    print('no directory passed')
    print('usage: {:s} /root/of/testdata'.format(sys.argv[0]))
    sys.exit(1)

data = splitSetIntoTrainingAndTest(readDataToTuples(sys.argv[1]))

for tag in data.testData:
    print('Current Tag: {:s}'.format(tag))
    print('\tTestset:')
    for item in data.testData[tag]:
        print('\t\t' + item)
    print('\tTrainingset:')
    for item in data.trainingData[tag]:
        print('\t\t' + item)


