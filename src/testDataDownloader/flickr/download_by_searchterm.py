from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from tqdm import tqdm
import threading
import requests
import math
import argparse
import codecs
import errno
import locale
import logging
import os
import sys
import time
import json

import flickrapi as Flickr
from flickrapi import FlickrError
import yaml

OAUTH_TOKEN_FILE = './.oauth'
DOWNLOAD_DIR = './download'
SORT_TYPE = 'interestingness-desc'


def main():
    """
    This is an image downloader programmed by Paul Hörmann.
    (c) Paul Hörmann
    """
    args = PARSER.parse_args()
    
    if len(sys.argv) < 2:
        PARSER.print_help()
        PARSER.exit()

    if not args.api_key or not args.api_secret:
        print('You need to pass in both "api_key" and "api_secret" arguments', file=sys.stderr)
        return 1

    flickr_service = Flickr.FlickrAPI(
        args.api_key, args.api_secret, username="hoermannpaul", format="parsed-json"
    )
    download_by_searchterm(
        args.searchterm, DOWNLOAD_DIR, flickr_service, args.page, args.number_of_items
    )
    if args.page_range:
        for page in range(args.page + 1, args.page + args.page_range):
            download_by_searchterm(
                args.searchterm, DOWNLOAD_DIR, flickr_service, page, args.number_of_items
            )
    return 0


def download_by_searchterm(term, download_dir, flickr_service, page, items_per_page):
    """
    Downloads images by searchterm
    @param term: str, The search term
    @param download_dir: str, The download directory
    @param flickr_service: str, The flickr service
    @param page: int, The pagenumber
    @param items_per_page: int, Number of items per page
    """
    prepare_download_dir(download_dir)
    found_images = get_imagelinks_by_searchterm(term, flickr_service, page, items_per_page)
    download_images_by_links(found_images, download_dir)
    return True


def prepare_download_dir(download_dir):
    """
    Prepares download directory
    @param dir: str, The directory to prepare
    """
    if not os.path.exists(download_dir):
        os.mkdir(download_dir)


def get_imagelinks_by_searchterm(term, flickr_service, page, items_per_page):
    """
    Gathers the imagelinks by a provided search term.
    @param term: str, The search term
    @param flickr_service: str, The flickr service
    @param page: int, The pagenumber
    @param items_per_page: int, Number of items per page
    """
    result = flickr_service.photos.search(
        text=term, sort=SORT_TYPE, per_page=items_per_page, page=page
    )
    images = result['photos']['photo']
    download_links = []
    for image_details in images:
        download_links.append(
            build_download_string(
                image_details['farm'],
                image_details['server'],
                image_details['id'],
                image_details['secret']
            )
        )
    return download_links


def build_download_string(farm_id, server_id, image_id, secret):
    """
    Builds the download link, the specs for how the link is built is from flickr doc.
    @param farm_id: str, The farm id
    @param server_id: str, The server id
    @param image_id: str, The image id
    @param secret: str, The secret of the image
    """
    prefix = 'https://farm'
    domain = '.staticflickr.com/'
    suffix = '_b.jpg'
    url = prefix + str(farm_id) + domain
    request_string = str(server_id) + '/' + str(image_id) + '_' + str(secret) + suffix
    download_link =  url +  request_string
    print('link: ' + download_link)
    return download_link


def download_images_by_links(image_links, download_dir):
    """
    Downloads and stores the images and provides progress report.
    @param download_dir: str, The download directory
    @param image_link: str, The image download link
    """
    for image_link in image_links:
        download_and_store_single_image_by_link(image_link, download_dir)


def download_and_store_single_image_by_link(image_link, download_dir):
    """
    Downloads and stores the image and provides progress report.
    @param download_dir: str, The download directory
    @param image_link: str, The image download link
    """
    filename = create_file_name(download_dir, image_link)
    if not os.path.exists(filename):
        r = requests.get(image_link, stream=True)
        # Total size in bytes.
        total_size = int(r.headers.get('content-length', 0))
        block_size = 1024
        wrote = 0
        if total_size > 20480:
            print('storing file ' + filename)
            with open(filename, 'wb') as f:
                for data in tqdm(r.iter_content(block_size), total=math.ceil(total_size//block_size) , unit='KB', unit_scale=True):
                    wrote = wrote  + len(data)
                    f.write(data)
        else:
            print('ignoring ' + filename + ' because of very small size')
    else:
        print(filename + ' already exists, skipping')


def create_file_name(download_dir, image_link):
    """
    Builds the filename.
    @param download_dir: str, The download directory
    @param image_link: str, The image download link
    """
    filename = download_dir + '/' + image_link.split('/').pop()
    return filename


# setup parser when script is started
if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='Downloads Flickr images by search term.\n'
        '\n'
        'To use it you need to get your own Flickr API key here:\n'
        'https://www.flickr.com/services/api/misc.api_keys.html\n'
        '\n'
        'For more information see:\n'
        'https://github.com/beaufour/flickr-download\n'
        '\n',
        epilog='examples:\n'
        '  list all sets for a user:\n'
        '  > {app} -k <api_key> -s <api_secret> -l beaufour\n'
        '\n'
        '  download a given set:\n'
        '  > {app} -k <api_key> -s <api_secret> -d 72157622764287329\n'
        '\n'
        '  download a given set, keeping duplicate names:\n'
        '  > {app} -k <api_key> -s <api_secret> -d 72157622764287329 -n title_increment\n'
        .format(app=sys.argv[0])
    )
    PARSER.add_argument('-k', '--api_key', type=str,
                        help='Flickr API key')
    PARSER.add_argument('-s', '--api_secret', type=str,
                        help='Flickr API secret')
    PARSER.add_argument('-a', '--user_auth', action='store_true',
                        help='Enable user authentication')
    PARSER.add_argument('-t', '--searchterm', type=str,
                        help='The searchterm to download images for')
    PARSER.add_argument('-p', '--page', type=int,
                        help='The number of the page')
    PARSER.add_argument('-r', '--page_range', type=int,
                        help='The number of pages to download, starting from page parameter')
    PARSER.add_argument('-n', '--number_of_items', type=int,
                        help='Number of items per page')
    main()
