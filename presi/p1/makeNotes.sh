#!/bin/bash

pandoc -o out.html -t html notes.md -s --metadata pagetitle="Notes"
pandoc -o out.pdf -t html5 out.html
