#!/bin/bash

pandoc -t revealjs -s -o myslides.html presentation.md -V revealjs-url=https://revealjs.com --metadata pagetitle="Automatic picture classification and analysis with AI-tools" -V theme=black
