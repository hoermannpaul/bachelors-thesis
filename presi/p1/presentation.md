# Automatic picture classification and analysis with AI-Tools

# about the topic
* suggestion by elements.at
* my priority 1

# overview
* motivation
* explanation
* goals
* status

# why?

#
* elements develop an open source platform Pimcore
* digital asset management
* lots of pictures

# what?

#
* automatically classify pictures and tag them
* utilizing pimcores tagging functionality 

# Testdata

#
## outside
![](./pics/img_de_deutsche_funkturm_13.JPG)

#
## lecture hall
![](./pics/img_at_wu_wien_112.jpg)

#
## foyer
![](./pics/img_at_falkensteiner_margaretengürtel_3.jpg)

#
## op
![](./pics/img_de_brainsuite_ulm_41.JPG)

# how?

# AI

#
* training with already tagged pictures

# status

# prototype
* working
* provided as installable pimcore bundle

# writing
* introduction: done
* basics: wip
* material: wip
* main: todo
* implementation: wip
* results: todo
* discussion: todo

# Questions?

# thank you for your attention
