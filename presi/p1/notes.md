## Intro
Guten Nachmittag, weil ich mir gedacht habe das ich nicht am ersten Termin will, und am letzten auch nicht
hab ich heute ausgewählt um euch ein bisschen etwas über das Thema meiner Bachelorarbeit zu erzählen. Da das
Thema Künstliche Intelligenz oder Artificial Intelligence viele englische Begriffe beherbergt schreibe ich
meine Arbeit in Englisch, und so wird auch der Rest dieser Presentation in Englisch stattfinden. Fragen
könnt ihr natürlich auch in Deutsch stellen.

## Automatische Bildklassifizierung und Analyse mit KI-Tools
or in english

## Automatic Picture Classification and Analysis using AI-Tools
Actually the reason I am presenting this that the title sound cooler in english.
The title is full of buzzwords, but don't be too excited, it is interesting,
but by far not as complex as the name might suggest.

## About
Now some quick facts about the topic. It was a suggestion by elements.at,
which is a company with their headquarters in Salzburg and a small office here in hagenberg.
I choose this topic with priority 1 ... uh you are all IT people ... priority 0 ... and got lucky.

## Why this topic?
I am intersted in AI since I recognized it's potential and also read a book
about the functionality for my english presentation. I also wanted to higher the chance
of an internship in a cool company.
(although I am working somewhere else now)

## Overview
To give you a little of an idea of what and when I am talking about today, here a little Overview.
After telling you something about the motivation for this topic suggestion I am going to explain you
the topic a bit more in detail followed by the goal of this thesis and in finally I am showing you
the current status of the thesis.

## Why?
Lets get started with the Why.
The reason is the platform Pimcore.
It is being developed by elements and is an open source platform for digital asset management.
It is mostly used for creating online shops or to show of the company portfolio.
Digital Asset Management means lots of pictures, thus a customer active in construction business
had a feature request to get an automatic tagging service.
Here an example of how the administration page of this platform looks like.

## What?
But what does this mean? A Solution to automatically classify pictures and tag them
utilzing pimcore's internal tagging ablity.

## Data

## How?

## Artifical Intelligence
I am using python with the tensorflow package because the community is very big
and tensorflow is a very powerfull tool.
The training of the network is done with already tagged training data. As you can see
the pimcore native tagging functionality is used and you can already see some example
tags I debugged with.

## Training Command
When you want to train a network you just have to give it the parent tag, modelname
and versionnumber of the training data and hit enter. After waiting a bit you are able to
execute network on a new picture.
This is the picture we are executing the command on.
That is the command and after the execution we can see that the correct tag was automatically
assigned and stored in the database.

## Goal

I already got the UI mock from elements and this is how the command should be executed once I am
done with the implementation. Though they offered me to implement the GUI on their own after I
implemented the service and provide me with a demo once it's done, to have some screenshots of
the final implementation.

## Status
### Prototype
As I told you earlier I am now telling you about my current progress with the thesis.
At first the prototype, it is working and providing basic functionality as training and tagging.
The Logic is provided as installable pimcore bundle and just has to be copied into an existing
pimcore installation.

### Writing
Whats about the thesis itself? I am already done with the introduction and started with the
chapters basics and material. The main chapter is still blank, whereas the implemenation chapter is
also work in progress. The final chapters results and discussion are still untouched and to be done
until february.

## End
I hope you now have an idea about my thesis and if there are any questions left, I would appreciate
it if they are asked.

Thank you for your attention!
