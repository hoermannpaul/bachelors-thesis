# Presentation Pimcore
## find existing and usable solutions
	> as I didn't want to spend the time to reinvent the wheel
* Google Vision API
* Amazon Tagging API
	> didn't see an easy way to reuse this
* retrain & label.py by Google
	> exactly what I hoped to find
## interface to pimcore
* retrain.py takes real files
	> first problem: retrain.py takes real files
	> ~convert tags to folders
* get location of images through pimcore command
* link files to training folders
	> no unnecessary copying / moving of data = fast & easy
## pretrained networks
* pretrained on image databases
	> variety of reference databases exit, can be a problem too
* spares lots of computational power and time
	> as the "heavy lifting" is done on suitable and very potent hardware
* less original data necessary
	> image databases contain lots of images, this lowers the risk of overfitting,
	> which means that the network looses it's ability to generalize
## problems
* licensing
	> most of them are for academic use only
* not for every use-case
	> lot's of noise because of potentially unneeded data
## imagenet
* http://www.image-net.org/
* 14,197,122 images
* 21,841 synsets (~categories)
	> as mentioned earlier: noise, in this case only 8 categories
## tensorhub
* https://www.tensorflow.org/hub/
* platform hosting machine learning modules
	> in a lot of different states, pretrained on different data sets
	> or not pretrained at all
* advanced architectures
* PNasNet: Progressive Neural Architecture Search
	> which means that while learning new architectures are tried
	> progressively, and thus is getting better over time
	> very good performance, but not easy to develop and design
# preprocessing
* greyscale
	> decreased performance quite a lot: makes sense: imagenet consists of colored images
* automatic down / upscaling to input size of network
	> done by the module itself, thus very flexible
## retraining
* additional layer on the output side
	> right most side, connected to the right most layer of the module
* "bottleneck" layer in Google terms
	> reduces / combines the features to the categories wanted
* complete network is only executed once and then cached
	> thus very fast, 6 Core CPU faster than a RTX 2080 although generally training
	> is way faster on a GPU
* only one layer needs training -> fast
	> cached output is getting fed into the layer, no execution
* network & label file gets stored in file system
	> simply by name & version
## tagging
	> simple, get image location again, feed into the script
* parsing output of label.py
* get tag id by tag name
* assigning tag with highest confidence
